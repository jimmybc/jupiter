﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AmmoCounter : MonoBehaviour {

    public int currentAmmo;
    public int maxAmmo;

    private Transform Weapon;
    private Transform Text;
	
    // Use this for initialization
	void Start () {
        Weapon = GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).transform.GetChild(1);
        Text = this.gameObject.transform;

        currentAmmo = Weapon.GetComponent<RaycastShoot> ().currentAmmo;
        maxAmmo = Weapon.GetComponent<RaycastShoot>().maxAmmo;
    }
	
	// Update is called once per frame
	void Update () {


        if (Weapon.gameObject.activeSelf)
        {

            Text.GetComponent<Text>().text = currentAmmo.ToString("N0") + "/" + maxAmmo.ToString("N0");
        }

        else
        {

            Text.GetComponent<Text>().text = ("");
        }
        }
    }
