﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class PlayerStatistics
{
    public int SceneID;
    public float PositionX, PositionY, PositionZ;

    public float MaxHealth;
    public float currentHealth;

    public float cash;
}

[Serializable]
public class SavedDroppableItem
{
    public float PositionX, PositionY, PositionZ;
}

[Serializable]
public class SavedDroppableList
{
    public int SceneID;
    public List<SavedDroppableItem> SavedItems;

    public SavedDroppableList(int newSceneID)
    {
        this.SceneID = newSceneID;
        this.SavedItems = new List<SavedDroppableItem>();
    }
} 

[Serializable]
public class PlaceableItem
{
    public bool Wasipickedup;
}

    
