﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelMaster : MonoBehaviour
{
    public Transform LevelGeometry;
    public GameObject CarcinosPrefab;
    public GameObject[] DespawnCandidates;


    // Use this for initialization

    private void Awake()
    {
        if (GlobalControl.Instance.IsSceneBeingLoaded || GlobalControl.Instance.IsSceneBeingTransitioned)
        {
            
        }
    }
    void Start()
    {

        
        GlobalControl.Instance.InitializeSceneList();

        if (GlobalControl.Instance.IsSceneBeingLoaded || GlobalControl.Instance.IsSceneBeingTransitioned)
        {
            GlobalControl.Instance.LoadInventory();
            


            SavedDroppableList localList = GlobalControl.Instance.GetListForScene();

            if (localList != null)
            {
                print("Saved objects count: " + localList.SavedItems.Count);

                for (int i = 0; i < localList.SavedItems.Count; i++)
                {
                    GameObject spawnedItem = (GameObject)Instantiate(CarcinosPrefab, LevelGeometry);
                    spawnedItem.transform.position = new Vector3(localList.SavedItems[i].PositionX,
                                                                 localList.SavedItems[i].PositionY,
                                                                 localList.SavedItems[i].PositionZ);

                }
            }

            else
                print("Local List was null.");
        }
    }
}

	
	// Update is called once per frame
	//void Update () {
		
	
