﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FSM.StateMachine;

public class NPCStateController : MonoBehaviour {

    public Transform eyes;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    [HideInInspector] public List<Transform> wayPointList;

    public enum States
    {
        Idle,
        Patrol,
        SawPlayer,
        ChasePlayer,
        AttackPlayer,
        Dialogue,
    }

    private StateMachine<States> fsm;

    // Use this for initialization
    void Awake () {

        navMeshAgent = GetComponent<NavMeshAgent>();

        fsm = StateMachine<States>.Initialize(this);

        fsm.ChangeState(States.Idle);
	}
	
    IEnumerator Idle_Enter()
    {
        yield return new WaitForSeconds(1);
    }
}
