﻿using UnityEngine;
using System.Collections;

public class UnitStats : MonoBehaviour
{

    //Stats for a target

    public string Objectname;
    public float MaxHealth = 3;
    public float currentHealth = 3;
    public float friendliness;

    
    private void Start()
    {
        
    }

    private void Update()
    {
        MaxHealth = Mathf.Clamp(currentHealth, 0, MaxHealth);
    }

    public void Damage(float damageAmount)

    {

        //subtract damage amount when Damage function is called

        currentHealth -= damageAmount;

        //Check if health has fallen below zero

        if (currentHealth <= 0)

        {

            //if health has fallen below zero, deactivate it 

            gameObject.SetActive(false);
        }

    }

}
