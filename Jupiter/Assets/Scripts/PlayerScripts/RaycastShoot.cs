﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShoot : MonoBehaviour {

    public int gunDamage = 1;
    public float fireRate = .25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;

    public int maxAmmo = 10;
    public int currentAmmo;
    public float reloadTime = 1f;

    public Transform gunEnd;
    private Camera fpsCam;
    private WaitForSeconds shotDuration = new WaitForSeconds(.07f);

    private LineRenderer laserLine;

    private float nextFire;


    // Use this for initialization
    void Start () {

        laserLine = GetComponent<LineRenderer>();
        fpsCam = GetComponentInParent<Camera>();

        if (currentAmmo == -1)
        currentAmmo = maxAmmo;
	}
	
	// Update is called once per frame
	void Update () {

        if (currentAmmo <= 0)
        {
            Reload();
            return;
        }

        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            Shoot();
        }
    }

    public void Shoot()
    {      
            currentAmmo--;

            nextFire = Time.time + fireRate;
            StartCoroutine(ShotEffect());
            Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));

            RaycastHit hit; // return hit

            laserLine.SetPosition(0, gunEnd.position);

            if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, weaponRange))
            {
                laserLine.SetPosition(1, hit.point);
                UnitStats health = hit.collider.GetComponent<UnitStats>();

                if (health != null)
                {
                    health.Damage(gunDamage);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * hitForce);
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * weaponRange));
            }       
    }

    void Reload ()
    {
        Debug.Log("Reloading...");
        currentAmmo = maxAmmo;
    }

    private IEnumerator ShotEffect()

    {

        laserLine.enabled = true;

        yield return shotDuration;

        laserLine.enabled = false;
    }
}
