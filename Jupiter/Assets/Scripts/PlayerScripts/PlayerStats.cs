﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class PlayerStats : MonoBehaviour
{
    public static PlayerStats Instance;

    public Transform playerPosition;

    //create a new PLaYER STATISTICS class from the Serializables script
    public PlayerStatistics localPlayerData = new PlayerStatistics();


      

    //Intialize weapon and inventory
    public GameObject currentWeapon;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (Instance != this)
            Destroy(gameObject);

        
    }

    //At start, load data from Global Control.
    void Start()
    {
        GlobalControl.Instance.Player = gameObject;

        localPlayerData = GlobalControl.Instance.savedPlayerData;
        playerPosition = this.gameObject.transform;

        //remove this stuff later, probably
        localPlayerData.MaxHealth = 3;
        localPlayerData.currentHealth = 3;
        localPlayerData.cash = 5.94f;
        //this too
        currentWeapon = this.gameObject.transform.GetChild(0).transform.GetChild(1).gameObject;

    }

    // Update is called once per frame
    void Update()
    {

        localPlayerData.MaxHealth = Mathf.Clamp(localPlayerData.currentHealth, 0, localPlayerData.MaxHealth);

    }

    public void Damage(float damageAmount)

    {

        //subtract damage amount when Damage function is called

        localPlayerData.currentHealth -= damageAmount;

        //Check if health has fallen below zero

        if (localPlayerData.currentHealth <= 0)

        {

            //if health has fallen below zero, deactivate it 

            gameObject.SetActive(false);
        }

    } 

    public void SavePlayer()
        {
        GlobalControl.Instance.savedPlayerData = localPlayerData;
        
    }
}
