﻿// Prints the name of the object camera is directly looking at
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RaycastExamine : MonoBehaviour
{
    private Camera cam;
    public float lookRange;
    public Text lookingAt;

    void Start()
    {
        lookRange = 20f;
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        Vector3 rayOrigin = cam.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));
        RaycastHit hit; // return hit

        if (Physics.Raycast(rayOrigin, cam.transform.forward, out hit, lookRange)) {
            UnitStats US = hit.collider.GetComponent<UnitStats>();
            ItemPickup IP = hit.collider.GetComponent<ItemPickup>();
            TransitionScript TS = hit.collider.GetComponent<TransitionScript>();

            if (US != null || IP != null || TS != null)
            {
                if (US)
                {
                    lookingAt.text = US.Objectname.ToString();                 
                }

                if (IP)
                {
                    lookingAt.text = IP.Objectname.ToString();

                    if (Input.GetButtonDown("Submit"))
                    {
                        IP.GetThisItem();
                        //if (IP.DefaultInventory = null)
                        //{
                         //   IP.DefaultInventory = this.gameObject.transform.parent.GetComponentInChildren<PGIModel>();
                        //}
                        Debug.Log("Getting e");
                    }
                } 

                if (TS)
                {
                    lookingAt.text = TS.Description.ToString();

                    if(Input.GetButtonDown("Submit"))
                    {
                        TS.UseTransition();
                        Debug.Log("Let's go.");
                    }
                }


            }

            if (!US && !IP && !TS)
            {
                lookingAt.text = "";
            }
        }
        //else
        //{
        //    lookingAt.text = "";
        //}
    }
}