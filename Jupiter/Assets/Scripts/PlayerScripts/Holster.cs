﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holster : MonoBehaviour {

    public bool Holstered;

    private Transform HeroFPPTransform;

    public Vector3 Idle;
    public Vector3 IdleRifle;
    public Vector3 IdlePistol;

    private Animator HeroFPPAnimator;

    private GameObject Weapon;

	// Use this for initialization
	void Start () {

        Holstered = true;

        Weapon = this.gameObject.transform.GetChild(0).transform.GetChild(1).gameObject;
        HeroFPPTransform = this.gameObject.transform.GetChild(0).transform.GetChild(0).transform;
        HeroFPPAnimator = this.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        //Holsters and unholsters weapon
        if (!Holstered && Input.GetKeyDown("h"))
        {
            Weapon.SetActive(false);
            HeroFPPAnimator.Play("Idle");
            Holstered = true;
            HeroFPPTransform.localPosition = Idle;
        }

        else if (Holstered && Input.GetKeyDown("h"))
        {
            Weapon.SetActive(true);
            HeroFPPAnimator.Play("IdlePistol");
            Holstered = false;
            HeroFPPTransform.localPosition = IdlePistol;
        }
	}
}
