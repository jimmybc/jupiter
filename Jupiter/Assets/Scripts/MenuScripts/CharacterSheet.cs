﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CharacterSheet : MonoBehaviour {

    //player
    private GameObject plyr;

    //stats
    private float MaxHealth;
    private float currentHealth;

    private float cash;

    //Character Sheet Children
    //Health
    private Transform Child0; //HP/MAX HP
    private Transform Child1; //Cash


    void Start()
    {
        Page1();
    }

public void Page1()
    {

        plyr = GameObject.FindWithTag("Player");

        //Get Values from Player
        MaxHealth = plyr.GetComponent<PlayerStats>().localPlayerData.MaxHealth;
        currentHealth = plyr.GetComponent<PlayerStats>().localPlayerData.currentHealth;

        cash = plyr.GetComponent<PlayerStats>().localPlayerData.cash;

        //Get first characterpanel child
        Child0 = this.gameObject.transform.GetChild(0); 
        //get second
        Child1 = this.gameObject.transform.GetChild(1);

        //Display stats
        Child0.GetComponent<Text>().text = currentHealth.ToString("N0") + "/" + MaxHealth.ToString("N0");
        Child1.GetComponent<Text>().text = "Cash: " + cash.ToString("N0");
    }
}
