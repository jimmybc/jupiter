﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMenus : MonoBehaviour {

    public GameObject Crosshair;
    public GameObject MenuCanvas;
    public GameObject Inventory;

    public static bool MenuOpened;

    // Update is called once per frame
    void Update () {

        if (Input.GetButton("Menu"))
        {
            //Open UI Submenu MenuCanvas
            this.MenuCanvas.SetActive(true);
            this.Crosshair.SetActive(false);
           
            //Stop Moving & Stop Looking
            FPSWalkerEnhanced move = this.gameObject.GetComponent<FPSWalkerEnhanced>();
            SimpleSmoothMouseLook look = this.gameObject.transform.GetChild(0).gameObject.GetComponent<SimpleSmoothMouseLook>();
            AimDownSights aim = this.gameObject.transform.GetChild(0).transform.GetChild(1).GetComponent<AimDownSights>();
            RaycastShoot shoot = this.gameObject.transform.GetChild(0).transform.GetChild(1).GetComponent<RaycastShoot>();

            move.enabled = false;
            look.enabled = false;
            aim.enabled = false;
            shoot.enabled = false;



            MenuOpened = true;
        }

        if (Input.GetButton("Cancel") && (MenuOpened == true))
        {
            //Open UI Submenu MenuCanvas
            this.MenuCanvas.SetActive(false);
            this.Crosshair.SetActive(true);

            //Stop Moving & Stop Looking
            FPSWalkerEnhanced move = this.gameObject.GetComponent<FPSWalkerEnhanced>();
            SimpleSmoothMouseLook look = this.gameObject.transform.GetChild(0).gameObject.GetComponent<SimpleSmoothMouseLook>();
            AimDownSights aim = this.gameObject.transform.GetChild(0).transform.GetChild(1).GetComponent<AimDownSights>();
            RaycastShoot shoot = this.gameObject.transform.GetChild(0).transform.GetChild(1).GetComponent<RaycastShoot>();

            move.enabled = true;
            look.enabled = true;
            aim.enabled = true;
            shoot.enabled = true;

            MenuOpened = false;
        }
    }
}
