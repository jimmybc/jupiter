﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;


public class ItemPickup : MonoBehaviour
{
    public string Objectname;
    

    // Use this for initialization
    void Awake()
    {
        
        
        
    }

    public void Start()
    {      
        GlobalControl.SaveEvent += SaveFunction;

    }

    public void GetThisItem()
    {

    }

    public void OnRemove()
    {
        GlobalControl.SaveEvent += SaveFunction;
    }

    public void SaveFunction(object sender, EventArgs args)
    {

    SavedDroppableItem item = new SavedDroppableItem();
    item.PositionX = transform.position.x;
    item.PositionY = transform.position.y;
    item.PositionZ = transform.position.z;
    
    GlobalControl.Instance.GetListForScene().SavedItems.Add(item);
      
    }

    public void OnDestroy()
    {
        GlobalControl.SaveEvent -= SaveFunction;
    }

}