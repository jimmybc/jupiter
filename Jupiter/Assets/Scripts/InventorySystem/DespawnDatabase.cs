﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "DespawnDatabase")]
public class DespawnDatabase : ScriptableObject {

    public bool dididothat = false;
    public List<PlaceableItem> database;
    

    void OnEnable()
    {
        if (database == null)
            database = new List<PlaceableItem>();
    }

    public void Add (PlaceableItem item)
    {
        database.Add(item);
    }

    public void Remove(PlaceableItem item)
    {
        database.Remove(item);
    }

    public void RemoveAt(int index)
    {
        database.RemoveAt(index);
    }

    public int COUNT
    {
        get { return database.Count; }
    }

    //.ElementAt() requires the System.Linq
    public PlaceableItem PlaceableItem(int index)
    {
        return database.ElementAt(index);
    }
}
